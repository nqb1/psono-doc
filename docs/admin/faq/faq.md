---
title: FAQ
metaTitle: FAQ | Psono Documentation
meta:
  - name: description
    content: FAQs concerning the installation and configuration
---

# FAQ

FAQs concerning the installation and configuration

[[toc]]


### Do you have an easier way to install Psono and try it out locally?

We offer our [quickstart](https://gitlab.com/psono/psono-quickstart) script that will setup a local demo environment in a couple of minutes.

::: warning
This script is only meant for demo purposes and not for a production grade system.
:::

### How to install the enterprise server without docker?

We currently do not provide the possibility to install the enterprise server without docker (baremetal). The baremetal installation
is meant for developers who can deal with python dependency issues and so on. So this has mainly two reasons:

1) The pain to update Psono regulary would be too high for administrators

2) The pain for us to deal with incorrect version libraries or system dependencies is too high for us to support.

### How to resolve "License not provided, and license servers unreachable"?

The psono enterprise server tries to connect to https://license01.psono.com on startup. It will send the PUBLIC_KEY to the server
and the license server responds with a license for 10 users. The error usually means that the server was unable to reach the license server.

You can check that the license server is available by opening this URL [https://license01.psono.com/info/](https://license01.psono.com/info/) in your browser.

This will most likely work indicating some kind of network (DNS, routing, ...) or firewall issue on your server.

You can check if the server in general can reach the license server with this command:

```bash
curl https://license01.psono.com/info/
```

::: tip
If this command fails, check with your network administrator. Be aware that license01.psono.com sits behind cloudflare so IP whitelisting is impossible.
:::

and this one to test from within your docker container:

```bash
docker run psono/psono-server-enterprise:latest curl https://license01.psono.com/info/
```

::: tip
If the first curl command worked and this one failed, then this is most likely a local missconfiguration. e.g. local firewall or iptables or docker networking issue.
:::

If both commands above work, then try with your actual settings.yml

```bash
docker run -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml psono/psono-server-enterprise:latest curl https://license01.psono.com/info/
```

::: tip
If you receive an error here, then this usually means that you have some kind of problem with your settings.yml. A potential cause could be cryptographic parameters (SECRET, PRIVATE or PUBLIC keys) not being generated with
the `python3 ./psono/manage.py generateserverkeys` command.
:::

### Can I run psono server enterprise offline without internet connection?

Yes, yet you have to do some additional configuration.

- Connection to license server: You can ask us to provide you an offline license code. Please send us an email to support@psono.com with your PUBLIC_KEY (it is part of your settings.yml) and we generate you an offline license code.

- Connection to a timeserver: Psono requires a NTP time server to be available. By default it is using time.google.com but it can be adjusted with the TIME_SERVER variable in your settings.yml

- Connection to Yubico Server: If you want to use YubiKey second factor you will have to make sure that your server can reach api*.yubico.com or confugure an own YubiKey server with the YUBICO_API_URLS variable

- Connection to Duo Server: If you want to use Duo second factor you will have to make sure that your server can reach api*.duosecurity.com.

### How do I fix "OCI runtime create failed"-Error?

This error usually has two potential reasons:

1) Incorrect path: Make sure that all paths you have specified in the docker command are correct.

2) Incorrect permissions: Make sure that no file permissions or SELINUX is blocking access to the files you mentioned in your command.

### How do I fix "non_field_errors"-Error when using SAML?

This error is extremely broad. The first thing that you should do is to enable DEBUG mode on your server with the settings.yml

Afterwards you should see a more detailed error.

### How do I fix "Invalid issuer in Assertion/Response ..."-Error when using SAML?

You have to adjust the entityId in your settings.yml to match the one from the error message. Afterwards don't forget to restart the server.

### How do I fix "Signature validation failed. SAML Response rejected"-Error when using SAML?

This error indicates that the IDP certificate that you configured in your settings.yml is incorrect.
Make sure that it has the right encoding (BASE64). After adjusting the settings.yml don't forget to restart the server.

### How do I fix "SAML_EMAIL_ATTRIBUTE_NOT_IN_ATTRIBUTES"-Error when using SAML?

You have to check the claims / attributes returned by the server to see if the name of the email attribute matches the one you configured in your SAML configuration in your settings.yml.

You can debug the response from the server by using one of many SAML debug extensions, e.g.:
[https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch](https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch)

Check that your "email_attribute" in your settings.yml matches the one that you see in the debug extension and don't forget to restart the server after adjusting the settings.yml

### How do I fix "SAML_USERNAME_ATTRIBUTE_NOT_IN_ATTRIBUTES"-Error when using SAML?

You have to check the claims / attributes returned by the server to see if the name of the username attribute matches the one you configured in your SAML configuration in your settings.yml.

You can debug the response from the server by using one of many SAML debug extensions, e.g.:
[https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch](https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch)

Check that your "username_attribute" in your settings.yml matches the one that you see in the debug extension and don't forget to restart the server after adjusting the settings.yml


