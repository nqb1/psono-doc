---
title: SAML - Group Mapping
metaTitle: SAML Group Mapping | Psono Documentation
meta:
  - name: description
    content: Configuration of the group mapping of SAML groups to Psono groups
---

# SAML Group Mapping

## Preamble

The EE server supports the SAML protocol that allows you to configure an external SAML IDP for authentication.
In addition the SAML server may provide groups. This guide here will explain how to map a SAML group to a Psono group
We assume that you already have configured SAML correctly with the necessary attribute configuration, so groups are
transferred proper. If not please check out the appropriate configuration guide. We further assume that you have a
working Admin Webclient running. If not please check out the [guide to install the admin client](/admin/installation/install-admin-webclient.html).

::: tip
This feature is only available in the Enterprise Edition.
:::

## Admin Webclient

1.  Login to the Admin webclient

    ![Login Admin Webclient](/images/admin/configuration/saml_group_mapping_login_portal.jpg)

2.  Create Managed Group

    Go to `Users` -> `Groups` and click the `+` button to create a managed group

    ![Create Managed Group](/images/admin/configuration/saml_group_mapping_create_managed_group.jpg)

3.  Edit Managed Group

    Go to `Users` -> `Groups` and click the `pencil` button next to the created group

    ![Create Managed Group](/images/admin/configuration/saml_group_mapping_edit_managed_group.jpg)

4.  Create Mapping

    Search for the SAML group and click the checkmark symbol in the "Mapped" column.

    ::: tip
    SAML groups are created on the fly whenever a user logs in, all his groups are imported. If the group that you are searching for is not here, please tell a user with this group to login.
    :::

    ![Create Mapping](/images/admin/configuration/saml_group_mapping_create_mapping.jpg)

5.  (optional) Grant Share Admin

    If you want to allow users of this SAML group to add new shares to this group, you have to grant them Share Admin.

    ![Grant Share Admin](/images/admin/configuration/saml_group_mapping_grant_share_admin.jpg)

    ::: tip
    It is considered best practise to share only one folder per group and add new entries or subfoldes to that one shared folder. That way all shares are instant and noone has to accept new shared secrets.
    :::

6.  Finished

    Whenever an user logs in with SAML, the server will map Psono groups according to the user's SAML groups and grant
    the user the necessary permissions. If a user loses access to a group the server will remove the user from the Psono
    group upon next login.


