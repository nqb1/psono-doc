---
title: Email - Mailjet
metaTitle: Email configuration with Mailjet | Psono Documentation
meta:
  - name: description
    content: Configuration of email delivery with Mailjet
---

# Email configuration with Mailjet

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use Mailjet for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    Restart the server afterwards

2.  Add Mailjet API credentials to setting.yml

	```
	EMAIL_BACKEND: 'anymail.backends.mailjet.EmailBackend'
    MAILJET_API_KEY: 'YOUR_MAILJET_API_KEY'
    MAILJET_SECRET_KEY: 'YOUR_MAILJET_SECRET_KEY'
	```

	- Replace `YOUR_MAILJET_API_KEY` with the API key that was provided to you by Mailjet.
	- Replace `YOUR_MAILJET_SECRET_KEY` with the secret that was provided to you by Mailjet.

    Restart the server afterwards


## Testing

To send a test email to `something@something.com` execute:

    python3 ./psono/manage.py sendtestmail something@something.com

or with docker:

    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/mailjet/](https://anymail.readthedocs.io/en/stable/esps/mailjet/)
