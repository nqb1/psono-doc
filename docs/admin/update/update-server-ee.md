---
title: 1b. Update Server EE
metaTitle: Update Psono Server EE | Psono Documentation
meta:
  - name: description
    content: Update instruction for the Enterprise Edition of the Psono server
---

# Update Psono Server EE

Update instruction for the Enterprise Edition of the Psono server

## Preamble

From time to time you want to update to the latest version. At this point we assume that you have created a backup of
your postgres database. If you have a possibility to snapshot your machine, please do so.

## Update with Docker

1.  Update the docker image

	    docker pull psono/psono-server-enterprise:latest

2.  Update the database

        docker run --rm \
          -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
          -ti psono/psono-server-enterprise:latest python3 ./psono/manage.py migrate

2.  Stop old psono-server-enterprise

        docker stop psono-server-enterprise

3.  Start new psono-server-enterprise

        docker run --name psono-server-enterprise-new \
            --sysctl net.core.somaxconn=65535 \
            -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
            -v /path/to/log/folder:/var/log/psono \
            -d --restart=unless-stopped -p 10100:80 psono/psono-server-enterprise:latest

4.  Cleanup

	If everything works you can cleanup your containers with the following commands:

		docker rm psono-server-enterprise
		docker rename psono-server-enterprise-new psono-server-enterprise

If anything fails you should be able to restore the database, and start the old docker container again.



