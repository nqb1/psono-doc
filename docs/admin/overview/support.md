---
title: Support
metaTitle: Support | Psono Documentation
meta:
  - name: description
    content: Contact us for any support issues.
---

# Support

## General

Let me know about any bugs or other issues that you find. Just create an issue here [directly within the GitLab repository](https://gitlab.com/psono/psono-client/issues) or email me at <a href="mailto:support@psono.com">support@psono.com</a>.

## Security Issues

To disclose security issues please use:

	security@psono.com

As of the time of writing our primary key has the fingerprint:

	5D2C B2BF 0675 7DEF 46A5 FCDD 0D8E ADBE B21F 162F

... with the following operational subkey fingerprint:

	35CD 9B42 C400 149D 27A3 F17B B932 C911 E5B1 BEA3

And can be found here:

[https://keyserver.ubuntu.com/pks/lookup?op=vindex&search=0x0D8EADBEB21F162F](https://keyserver.ubuntu.com/pks/lookup?op=vindex&search=0x0D8EADBEB21F162F)

Our PGP Public Key:

	-----BEGIN PGP PUBLIC KEY BLOCK-----
	Version: Psono v1
	Comment: https://psono.com

	xsFNBFpHxO4BEADhWI6AqIOc9Vgd0Qq/yi7CcGlZ8IkJYyQF3cHqpDjRxtJ3
	QMYK5uGOrzBhWoIcMXXugSqmIhCt0R4cHgI406WzYkRY+dCNrIUCnWMWg4Jr
	aGI8K5yrYNISSWtzNGRdGe0rLQsY4EnbDKzbkQJJxoQX5MojzMDwYFEoOUXi
	6y5OiMmxRTPdV7WNOtjvWYNE8N1YJP+/QKoZwuPq32tRvdPZT2H0pozUDyZo
	EOESxKNsD5LZ4oYL7fYtTsO0vsatYvaWT8EonbWXVRDMcHSqMAP0ydtXyS8s
	jmb6TzKtv+J2tGVszEi9XBQUa79TIfJwGwFr8sUK+ndP/xVPXjwB4M1lTQOn
	Ig7PaXNSuhbS8sXhgeYjSohrlZFAzpd6eeqw/H3noQCtbRrAc6berZTSsRy2
	yRB6nR7irkyjZYg7pGW3FAFBXXxQx21gjFPyQdUe1oMHDngUlhOawuJ8AzOE
	G9dg7igKorMXtWcefmoewKIiHn4iz6YFMpQ983xUy7BE+n/Vdq/g9+rl0plQ
	By5oOKWqube6qf5xQsAEwx69nJLd+BMTyDGPxD3xT6or9QX8hB9lHPPlHf7y
	m/ueUI6mybFsoubbqRhnc9jWsG4hBgxSaeeXAvM9UC0LbxXGgJavmZcBsEDn
	+EHvzOcjLHgswLk/XZHTIwSf3NX+fPincmpBwwARAQABzSNQc29ubyBTZWN1
	cml0eSA8c2VjdXJpdHlAcHNvbm8uY29tPsLBdQQQAQgAKQUCWkfE7wYLCQcI
	AwIJEA2Orb6yHxYvBBUICgIDFgIBAhkBAhsDAh4BAAD0Ng//Rv7I/wwd7l1E
	EPChSUpt92p1PpZVIaUlSnBdVKu0g58K85wtHyX1OmDHJRWVON56U4rfP5Hr
	nn/rOqpXdSyM7djTtcVxvGNJf7rAS/7s9WR19YS5NfIC3LiAJ5NtjXQcjd3y
	PpHh7LATmWGYob3WSWjyies2TLfRaV8Tlppa+UjVQOKNneOV1nvMrGCO6tw2
	M9Ak0IU0oYMB306CaTAFbfGuLMTtA97eKArb1BgcW71bqsPeFRb//ENPJ+xu
	qy+ZXVFivGoJef+eCmCFkrXXVd9Di06ZSxCxE0J7DLU/VUrcxPg1Z24gRSOb
	fzC6VOGXvfWbSCrL3P34wfdaymmw4VwOuuqcBo9eAVulpbeb1NPKdlQzLP/L
	576dsub1iPaJs2CKzqjujMQKKx5C4WsTDLsacyOLMv9Lx7UK/HHeWbAFrfmo
	NnHlRc0RoSlJtGRgebi9cLLutrchqvf/fmGIe9CvjmDn/bpyMD3LEcBngCww
	YlaZsBLZRpL0UO/+vw5/C37zbtz6r3d9XbBb0FAN3A5WuPpcun8bgyrbSfPu
	liIRC0IFR7pDYmAksrZZfZ8JSa3iAimRVa0tMlR8b5YbC6FQAMghYwoXCsl/
	aphU6hnO2/vzdOHj4Hgejd11kVTyLErF448mXU75czmsFVsMqemRLP6mYNze
	q4GJgcxGqI7OwU0EWkfE7gEQAKuQn4jZrjO/6JN0IjiWwUoiJn7gdHhb69eF
	JutgJaU4loGknLSjj9QCH3hBY2j7BYSBU4XG9FRuBd34n0Vkd2b1ZFpsZ9bK
	/XdiXiYeuRUa+drGY7idJyi5GC8y3ju06RUKevxOI3Tec44XShvrLk2XYaAu
	4t8ej6J59IIBcuNwNsd+1E2ekgxDrv32g6vp32UgYMiEds72x5jZwVSsJXap
	9fVtbNmyWf/3xd/fownuyqijajRQ1Z9DkSzxz5EPPlClVNlS07vBtvKQ5Lny
	i0h2Yny4Al8atm14OcuHKNTjTS+V/2yjv5ttk8u2dTxkN7MRNdqeUEs0I7ns
	ENyg4FNNhKiXCQEG9Plu3JYboVGSWstOdyvtETejfljXq5eiKPcVcsDUc6B/
	zoRT+FGsp+8hI2m/YDKt1DgYtXKL1VlSrPc2gFU8YVNNEQbFcJuvF4ydW59/
	Qt3a8/FsE2xf8xo8yHRMpQXuDya6tnxDUSeejTnyNCudGDRvcFz/M8TPg/ZW
	/AO8mx1EHH4kLzM/Lku3YPCklwMe0erP47XM4nSJUdHPnu3edfaYFoEmZ7xa
	nJ9/PBKFRlgAxf1hcVlCqutDP+inbq6pIymXioKEeOK9x3UkwdBOf4ioTaVM
	LYVbcktoYOg0n9tGo1jtJEEjaGeFHZo6eY72dHvyeO7oHNCbABEBAAHCwV8E
	GAEIABMFAlpHxPAJEA2Orb6yHxYvAhsMAADqeA//e6XKOJiRZcwsiWSCHGfA
	8gO0zvG70R4JqDXX1ekp8VAd/7HNWwPkI3fYcrEdJ5//Qlo3nntBWaqhLGjg
	tqJTx2rTfXKvw67kv7wZX2AuHFrdJneqz2xFyJ2rfkw+eWMO25k13deirdxq
	qaG/XTDfSX3hpuw7i815KY7Fhp8xmI44NFNcEHcGRX2hclTylvg6Bk7AUtL7
	4Rz4InDB7+VaoUT9mO8x8oVNz+V/JP06qff0hJVJnBo6EpWoArog40j9isLG
	VN5ddjzAT+jpBCxQyjX51eNuJCjdZ3Ht3VxiRi0EED+nsvXHDgzR4TOVPuhI
	bRZXIx6jPwr88JLeuVHR4OYqVzbQJmLEwxzU7hvrzNSrHx7EvihUF/o7Q71e
	SEgtdYNXkjKMVYxg3/LRSPn14Kp/dbzp1lS7xQpgiUcEdbzE+29UZFtOyfiD
	IjvBpjP1aZIDQf/JB3JGJCQiWDGZO3sbAL4wbwZQYOiuZ3aWUli8DfC1TWeJ
	R0AB2CwX+RkssKqMszX1QKu4QYjzzmNUw0ieQMw3eZuEq3DF21LCznzBJs6e
	AWIUWNIBvb04V7mK7QNKS7YwrEKGl58OVrqd8EDXgM05xYwH5LnXGij1FRf/
	F3KZLqN6hqp+jk1XywlRwFJl+4B8lDydPtzs1fBHfUyv6ZD90pMi7R2YBL2c
	hRE=
	=e+cy
	-----END PGP PUBLIC KEY BLOCK-----

